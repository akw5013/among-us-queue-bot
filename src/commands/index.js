const logger = require('../utils/logger')
const sendMessage = require('../utils/send-message')
const sendDirectMessage = require('../utils/send-direct-message')
const parseMessageArgs = require('../utils/parse-message-args')
const updateMessages = require('../utils/update-channel-messages')
const gameCommands = require('./game')
const REFRESH_INTERVAL = 1000 * 60
const monitoredChannels = []

module.exports = (client) => {
    const commandList = Object.assign({}, ...[gameCommands])
    client.on('message', async (message) => {
        // Make sure the bot never tries to respond to itself
        if (message.author.id === client.user.id) { return }
        // Only monitor the specified channels
        if(!monitoredChannels.includes(message.channel.id)) { return }
        const commandArgs = parseMessageArgs(message)
        if (!commandArgs || commandArgs.length === 0) { return }
        const [commandName, ...args] = commandArgs
        const command = commandList[commandName]
        if (command) {
            logger.logInfo(`${message.author.username}(${message.author.id}) - ${message.content}`)
            const response = await command(message, args)
            logger.logDebug(response)
            if (response.message) {
                if (response.dm) {
                    sendDirectMessage(message.author, response.message)
                } else {
                    sendMessage(message.channel, response.message)
                }
            }
            if (response.shouldUpdate) {
                updateMessages(message.channel)
            }
        }
        message.delete()
    })

    client.on('ready', () => {
        logger.logInfo(`Connected as ${client.user.tag}`)
        client.guilds.cache.each((guild) => {
            logger.logInfo(` - ${guild.name} - ${guild.id}`)
            createDefaultChannel(guild)
        })
    })

    // called guildCreate but fires when the client joins a guild
    client.on('guildCreate', (guild) => {
        createDefaultChannel(guild)
    })

    client.on('error', logger.logError)
}

const findChannel = async (guild) => {
    return guild.channels.cache.find(g => g.name === process.env.CHANNEL_NAME)
}

const createDefaultChannel = async (guild) => {
    // Create necessary channels
    logger.info(`Checking for channel called ${process.env.CHANNEL_NAME}`)
    let existingChannel = await findChannel(guild)
    if (!existingChannel) {
        logger.info('Creating new channel')
        existingChannel = await guild.channels.create(process.env.CHANNEL_NAME)
    }
    if (existingChannel) {
        logger.info(`Updating messages in ${existingChannel.guild.name}`)
        updateMessages(existingChannel)
        // Set polling to update messages
        setInterval(() => {
            logger.info(`Updating messages in ${existingChannel.guild.name}`)
            updateMessages(existingChannel)
        }, REFRESH_INTERVAL)
        monitoredChannels.push(existingChannel.id)
    }
}
