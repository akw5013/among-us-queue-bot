const getActiveQueue = require('../../data/get-active-queue')
const saveData = require('../../data/save-data')
const sendDirectMessage = require('../../utils/send-direct-message')

module.exports = async (message, _args) => {
    const queue = await getActiveQueue(message.channel)
    if (!queue.games) {
        queue.games = []
    }
    if (!queue.players) {
        queue.players = []
    }
    const player = queue.players.find(p => p.id === message.author.id)
    if (!player) {
        return { message: 'You are not in the queue.', shouldUpdate: false, dm: true }
    }
    const activeGame = queue.games.find((game) => game.players.find((p) => p.id === player.id))
    let activeGameNumber = 0
    if (activeGame) {
        activeGameNumber = activeGame.number
        activeGame.players.splice(activeGame.players.indexOf(activeGame.players.find((p) => p.id === player.id), 1))
        if (activeGame.players.length === 0) {
            queue.games.splice(queue.games.indexOf(activeGame, 1))
            const gameMsg = await channel.messages.fetch(activeGame.messageId)
            gameMsg.delete()
        }
    }
    const { position } = player
    queue.players.splice(queue.players.indexOf(player), 1)
    queue.players.forEach((p) => {
        if (p.position > position && (!p.game || p.game === activeGameNumber)) {
            p.position -= 1
            if (p.position === 0) {
                sendDirectMessage({ id: p.id }, 'Your spot is ready for a game of Among Us!  Please type `!accept` in the among-us-queue channel to accept it or `!leave` to remove yourself from the queue!')
            }
        }
    })
    saveData()
    return { message: '', shouldUpdate: true }
}