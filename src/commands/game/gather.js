const sendMessage = require("../../utils/send-message")
const MINUTES = 15
const ALERT_TIME = 1000 * 60 * MINUTES
let activeAlert = null
module.exports = async (message, _args) => {
    if (activeAlert) {
        return { message: 'There is already an active ping.  Be patient!', shouldUpdate: false, dm: true }
    }
    const pingRole = message.guild.roles.cache.find(r => r.name === process.env.PING_ROLE)
    if (pingRole) {
        activeAlert = await sendMessage(message.channel, `${pingRole} - ${message.member.nickname || message.author.username} is looking for crewmates.`)
        setTimeout(() => {
            activeAlert.delete()
            activeAlert = null
        }, ALERT_TIME)
    }
    return { message: '', shouldUpdate: false, }
}