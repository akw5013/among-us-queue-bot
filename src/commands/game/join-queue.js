const getActiveQueue = require('../../data/get-active-queue')
const saveData = require('../../data/save-data')
const moment = require('moment')
const sendDirectMessage = require('../../utils/send-direct-message')
const MAX_PLAYERS = 10

module.exports = async (message, args) => {
    const queue = await getActiveQueue(message.channel)
    const [number] = args
    let safeNumber = Number(number)
    if (isNaN(safeNumber) || safeNumber < 0) {
        safeNumber = 0
    }
    if (!queue.games) {
        queue.games = []
    }
    if (!queue.players) {
        queue.players = []
    }
    if (queue.players.some(p => p.id === message.author.id)) {
        return { message: 'You are already in the queue.', shouldUpdate: false, dm: true }
    }
    queue.players.push({
        id: message.author.id,
        name: message.member.nickname || message.author.username,
        status: 'Waiting',
        entryTime: moment().toISOString(false),
        game: safeNumber,
        position: queue.players.filter(p => p.status === 'Waiting').length + 1
    })
    queue.players.forEach((p) => {
        const game = queue.games.find(g => !p.game || p.game === g.number)
        if (game.players.length < MAX_PLAYERS) {
            const numPlayers = MAX_PLAYERS - game.players.length
            if (p.position > 0 && p.position <= numPlayers) {
                p.position = 0
                sendDirectMessage({ id: p.id }, 'Your spot is ready for a game of Among Us!  Please type `!accept` in the among-us-queue channel to accept it or `!leave` to remove yourself from the queue!')
            }
        }
    })
    let start = 1
    queue.players.sort((p1, p2) => p1.position - p2.position).forEach((p) => {
        if (p.position !== 0) {
            p.position = start
            start++
        }
    })
    saveData()
    return { message: '', shouldUpdate: true }
}