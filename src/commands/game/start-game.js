const getActiveQueue = require('../../data/get-active-queue')
const saveData = require('../../data/save-data')
const moment = require('moment')
const MAX_PLAYERS = 10
module.exports = async (message, args) => {
    const queue = await getActiveQueue(message.channel)
    if (!queue.games) {
        queue.games = []
    }
    const [code, map] = args
    if (!code || !map) {
        return { message: 'Please provide a code and a map.', shouldUpdate: false, dm: true }
    }
    const players = queue.players.filter(p => p.position > 0 && p.position <= MAX_PLAYERS)    
    queue.games.push({
        number: queue.games.length + 1,
        startTime: moment().toISOString(false),
        map: map,
        code: code,
        players: players,
    })
    players.forEach((player) => {
        player.status = 'Playing'
        player.playingTime = moment().toISOString(false)
        player.position = 0        
    })
    
    saveData()
    return { message: '', shouldUpdate: true }
}