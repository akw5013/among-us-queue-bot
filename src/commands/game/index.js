const joinQueue = require('./join-queue')
const leaveQueue = require('./leave-queue')
const startGame = require('./start-game')
const editGame = require('./edit-game')
const endGame = require('./end-game')
const gather = require('./gather')
const togglePing = require('./toggle-ping-role')
const acceptGame = require('./accept-spot')

module.exports = {
    '!join': joinQueue,
    '!leave': leaveQueue,
    '!start': startGame,
    '!edit': editGame,
    '!endgame': endGame,
    '!gather': gather,
    '!toggleping': togglePing,
    '!accept': acceptGame,
}