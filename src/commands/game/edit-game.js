const getActiveQueue = require("../../data/get-active-queue")
const saveData = require("../../data/save-data")

module.exports = async (message, args) => { 
    const [gameNumber, code, map] = args
    if (!gameNumber) {
        return { message: 'Did you forget a game number?', dm: true }
    }
    const queue = await getActiveQueue(message.channel)
    const game = queue.games.find(game => game.number === Number(gameNumber))
    if (!game) {
        return { message: `No game with number ${gameNumber} found.`, dm: true }
    }
    game.code = code
    game.map = map
    saveData()
    return { message: '', shouldUpdate: true, dm: false }
}