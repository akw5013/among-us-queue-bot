const getActiveQueue = require("../../data/get-active-queue")
const saveData = require("../../data/save-data")

module.exports = async (message, _args) => {
    const queue = await getActiveQueue(message.channel)
    if (!queue.players) {
        queue.players = []
    }
    const player = queue.players.find(p => p.id === message.author.id)
    if (!player) {
        return { message: 'You are not in the queue!', shouldUpdate: false, dm: true }
    }
    if (player.priority > 1) {
        return { message: 'It\'s not your turn!', shouldUpdate: false, dm: true }
    }
    const game = player.game || 0
    const theGame = queue.games.find(g => g.game === game || game === 0)
    player.status = 'Playing'
    player.playingTime = moment().toISOString(false)
    player.position = 0
    theGame.players.push(player)
    saveData()
    return { message: '', shouldUpdate: true }
}