module.exports = async (message) => {
    const { member } = message
    if (!member) {
        return { message: 'This command can only be used on an active server of which you are a member.', dm: true }
    }
    const pingRoleName = process.env.PING_ROLE
    if (!pingRoleName) {
        return { message: 'No ping role has been configured. Blame DarkPaladin', dm: true }
    }
    const pingAlertRole = await message.guild.roles.cache.find(role => role.name === pingRoleName)
    if (!pingAlertRole) {
        return { message: `No role named ${pingRoleName} was found. Please check with an admin to make sure the server is configured correctly.`, dm: true }
    }
    if (await member.roles.cache.get(pingAlertRole.id)) {
        try {
            const result = await member.roles.remove(pingAlertRole)
            if (result) {
                return { message: `You have been successfully removed from the ${pingRoleName} role.`, dm: true }
            }
        } catch (e) {
            return { message: 'An error has occurred while updated roles. Please check that the bot has permissions', dm: true }
        }
    }
    try {
        const result = await member.roles.add(pingAlertRole)
        if (result) {
            return { message: `You have been successfully added to the ${pingRoleName} role.`, dm: true }
        }
    } catch (e) {
        return { message: 'An error has occurred while updating roles. Please check that the bot has permissions', dm: true }
    }
    return { message: 'An unknown error has occurred.  Please inform DarkPaladin.', dm: true } // Should not actually happen
}