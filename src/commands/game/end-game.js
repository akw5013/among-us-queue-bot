const getActiveQueue = require("../../data/get-active-queue")
const saveData = require("../../data/save-data")

module.exports = async (message, args) => {
    const [gameNumber] = args
    if (!gameNumber) {
        return { message: 'Did you forget a game number?', dm: true }
    }
    const queue = await getActiveQueue(message.channel)
    const game = queue.games.find(game => game.number === Number(gameNumber))
    if (!game) {
        return { message: `No game with number ${gameNumber} found.`, dm: true }
    }
    queue.players = queue.players.filter(p => {
        return !game.players.includes(p)
    })
    const gameMsg = await channel.messages.fetch(game.messageId)
    gameMsg.delete()
    queue.games.splice(queue.games.indexOf(game), 1)
    await saveData()
    return { message: '', shouldUpdate: true, dm: false }
}