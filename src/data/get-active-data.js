const fs = require('fs')
const path = require('path')
let activeData = null

module.exports = () => {
    if (!activeData) {
        activeData = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '..', 'temp_storage', 'active_games.json')))
    }
    return activeData
}