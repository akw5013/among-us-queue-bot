const getActiveData = require('./get-active-data')

module.exports = async (channel) => {
    const data = await getActiveData()
    if (!data[channel.id]) {
        data[channel.id] = { queue: { 
            welcomeMessage: 0,
            statusMessage: 0,
            games: [],
            players: [],
        } }
    }    
    return data[channel.id].queue
}