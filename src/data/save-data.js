const fs = require('fs')
const path = require('path')
const getActiveData = require('./get-active-data')

module.exports = async () => {
    const activeData = getActiveData()
    return new Promise((res, rej) => fs.writeFile(path.join(__dirname, '..', '..', 'temp_storage', 'active_games.json'), JSON.stringify(activeData), (err) => {
        if (err) {
            rej(err)
        }
        res()
    }))
}