module.exports = () => {
return `
Welcome to Among-Sus-Bot!
Please use commands responsibly, there's no security on them.
\`\`\`
Commands:
    * !join [num] - Joins the active queue, or a specified game queue.
    * !leave - Leaves the active queue/game.
    * !start code map - Starts a new game.
    * !edit num code map - Edits details for an active game
    * !endgame num - Ends a game.
    * !gather - Sends a ping to the ping role to gather interest for a game.
    * !accept - Accepts your spot when it is your turn in the queue.
    * !toggleping - Adds or removes yourself to the ping role.
\`\`\`
`
}