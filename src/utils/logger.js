const logger = require('../../config/winston')

const logInfo = (message) => {
    logger.info(message)
}
const logWarn = (message) => {
    logger.warn(message)
}
const logError = (message) => {
    logger.error(message)
}
const logDebug = (message) => {
    logger.debug(message)
}
module.exports = {
    logInfo,
    logWarn,
    logError,
    logDebug,
    warn: logWarn,
    info: logInfo,
    error: logError,
    debug: logDebug,
}
