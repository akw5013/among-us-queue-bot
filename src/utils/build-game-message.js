const buildTable = require('./build-table')
const moment = require('moment')

const determineTime = (game) => {
    const now = moment()
    return `${moment.duration(now.diff(moment(game.startTime))).get('minutes')} minutes`
}

module.exports = (game) => {
    const headers = ['Number', 'Length', 'Code', 'Map', 'Players']
    const data = [{
        number: game.number,
        length: determineTime(game),
        code: game.code,
        map: game.map,
        players: game.players.map(p => p.name).join(', ')
    }]
    return `\`\`\`\n${buildTable(headers, data)}\n\`\`\``
}