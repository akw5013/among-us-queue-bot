const buildTable = require('./build-table')
const moment = require('moment')

const determineTime = (player) => {
    const now = moment()
    let startingMoment
    if(player.status === 'Waiting') {
        startingMoment = moment(player.entryTime)
    } else {
        startingMoment = moment(player.playingTime)
    }
    return `${moment.duration(now.diff(startingMoment)).get('minutes')} minutes`
}

module.exports = (queue) => {
    const headers = ['Name', 'Position', 'Time', 'Game', 'Status']
    const data = queue.players.map((player) => ({
        name: player.name,
        position: player.position,
        time: determineTime(player),
        game: player.game || 'No Preference',
        status: player.status,
    }))
    return `\`\`\`\n${buildTable(headers, data)}\n\`\`\``
}