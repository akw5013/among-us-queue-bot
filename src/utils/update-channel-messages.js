const getQueue = require('../data/get-active-queue')
const buildWelcomeMessage = require('./build-welcome-message')
const buildQueueMessage = require('./build-queue-message')
const buildGameMessage = require('./build-game-message')
const saveData = require('../data/save-data')
const sendMessage = require('./send-message')
const logger = require('./logger')

module.exports = async (channel) => {
    const queue = await getQueue(channel)
    let { welcomeMessage, statusMessage, games } = queue
    const welcomeText = buildWelcomeMessage()
    if (!welcomeMessage) {
        logger.info('Creating welcome message')
        welcomeMessage = (await sendMessage(channel, welcomeText)).id
        queue.welcomeMessage = welcomeMessage
    } else {
        const message = await channel.messages.fetch(welcomeMessage)
        message.edit(welcomeText)
    }
    const statusText = buildQueueMessage(queue)
    if (!statusMessage) {
        logger.info('Creating status message')
        statusMessage = (await sendMessage(channel, buildQueueMessage(queue))).id
        queue.statusMessage = statusMessage
    } else {
        const message = await channel.messages.fetch(statusMessage)
        message.edit(statusText)
    }
    if(games) {
        await Promise.all(games.map(async (game) => {
            const gameMessage = buildGameMessage(game)
            if (!game.messageId) {
                logger.info('Creating game message')
                game.messageId = (await sendMessage(channel, gameMessage)).id
            } else {
                const message = await channel.messages.fetch(game.messageId)
                message.edit(gameMessage)
            }
            return
        }))
    }
    saveData()
}