require('dotenv').config()
const client = require('./bot')

// https://discordapp.com/api/oauth2/authorize?client_id=770298787364864001&scope=bot&permissions=268446800
client.login(process.env.DISCORD_CLIENT_TOKEN)