const { createLogger, format, transports } = require('winston')
const path = require('path')

const LOG_REL_PATH = './logs'
const options = {
    file: {
        level: 'debug',
        filename: `${path.resolve(LOG_REL_PATH)}/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242800,
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'info',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
}
const logger = createLogger({
    level: 'info',
    format: format.combine(
        format.colorize(),
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
    ),
    transports: [new transports.File(options.file), new transports.Console(options.console)],
})
module.exports = logger
